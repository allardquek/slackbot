"""
Script to pickle csv files into dictionaries, which can then be loaded into memory for use.
Use cases:
	1. Pickle questions csv file
	2. Pickle jokes csv file (in a similar question-answer format)
"""
import csv
import pickle
import random
# from helpers import preprocess


def index_file(input_file, output_file):
	dictionary = {}
	with open(input_file, mode='r') as f:
		rows = csv.reader(f)
		next(rows)                  # Skip the header row   

		for row in rows:
			question, ans = row[0], row[1]
			# Preprocess question if is an FAQ
			# question = preprocess(question)

			dictionary[question] = ans
			print("Q:", question)
			print("A:", ans, '\n')

	with open(output_file, 'wb') as f:
		pickle.dump(dictionary, f)

	print("Done pickling csv file!")


if __name__ == '__main__':
	input_file = 'data/jokes.csv'
	output_file = 'data/jokes.txt'
	index_file(input_file, output_file)
	
	# Show sample joke
	with open (output_file, 'rb') as f:
		dictionary = pickle.load(f)
	c = random.choice(list(dictionary.keys()))
	print(c)
	print(dictionary[c])
