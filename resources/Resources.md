# Resources

Find all relevant resources and approaches considered to build this Slackbot.

## Links

- [Chatterbot](https://chatterbot.readthedocs.io/)
- [Spacy Text Similarity](https://spacy.io/usage/linguistic-features/#vectors-similarity): Read expectations [here](https://spacy.io/usage/linguistic-features/#similarity-expectations) for more
- [FuzzyWuzzy](https://www.analyticsvidhya.com/blog/2021/06/fuzzywuzzy-python-library-interesting-tool-for-nlp-and-text-analytics/)
- Question Answering (QA) [Demo](https://demo.allennlp.org/reading-comprehension/bidaf-elmo)

### Other Tutorials

- https://www.educative.io/courses/build-your-own-chatbot-in-python/qAA5mKY19Gp
- https://towardsdatascience.com/beginners-guide-to-creating-a-powerful-chatbot-48fc6b073e55
- https://www.upgrad.com/blog/how-to-make-chatbot-in-python/
- https://www.frontiersin.org/articles/10.3389/frai.2021.654924/full
- https://chatbotslife.com/understand-how-rasa-framework-works-81d5a9afeb22
- https://towardsdatascience.com/building-a-conversational-ai-chatbot-with-aws-lambda-function-and-amazon-efs-615fddb4d55
- https://itsromiljain.medium.com/build-a-conversational-chatbot-with-rasa-stack-and-python-rasa-nlu-b79dfbe59491
- https://www.analyticsvidhya.com/blog/2021/11/an-introduction-to-chatbot-development-using-rasa/
