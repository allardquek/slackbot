"""
This script demonstrates how to obtain the actual textual answer given the answer id 1.
"""
import pandas as pd

ans_id = 1
df = pd.read_csv('answers.csv').to_dict()
print(df['text'][ans_id - 1])
