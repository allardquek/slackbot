"""
This script demonstrates how to make an API request to get the joke of the day.
"""
import requests


url = 'https://api.jokes.one/jod?category=knock-knock'
headers = {'content-type': 'application/json'}

response = requests.get(url, headers=headers)
#print(response)
#print(response.text)
jokes=response.json()['contents']['jokes'][0]
print(jokes['joke']['text'])