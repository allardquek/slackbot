"""
Evaluation script to test how many questions the bot can respond to correctly.
"""
import csv
import logging
from chatterbot import ChatBot
from helpers import preprocess, MIN_CONFIDENCE, DEFAULT_REPLY


def eval_bot():
    logging.basicConfig(filename='eval.log',level=logging.INFO)

    # Load trained bot from db
    bot = ChatBot(
        'Ai Palette Bot',
        storage_adapter='chatterbot.storage.SQLStorageAdapter',
        preprocessors=[
            'chatterbot.preprocessors.clean_whitespace',
        ],
        logic_adapters=[
            {
                'import_path': 'chatterbot.logic.BestMatch',
                'default_response': DEFAULT_REPLY,
                'maximum_similarity_threshold': MIN_CONFIDENCE,
                # 'statement_comparison_function': chatterbot.comparisons.levenshtein_distance,
                # 'response_selection_method': chatterbot.response_selection.get_first_response
            },
            'chatterbot.logic.MathematicalEvaluation'
        ],
        database_uri='sqlite:///db.sqlite3',
        read_only=True
    )


    # Read in each question from csv file
    with open('questions.csv') as csvfile:
        rows = csv.reader(csvfile)
        next(rows)                  # Skip the header row   
        TEST_DATA = list(rows)

    print("Number of test inputs:", len(TEST_DATA))
    num_correct = 0

    for test in TEST_DATA:
        # Use trained bot to answer questions
        qn, ans = test[0], test[1]
        processed_qn = preprocess(qn)

        response = bot.get_response(processed_qn, read_only=True).text
        print(f"Expected: {ans}, Actual: {response}")

        # Check if response is correct 
        if ans == response:
            num_correct += 1

    accuracy = num_correct / len(TEST_DATA)
    print(f"DONE: {num_correct} / {len(TEST_DATA)} ({accuracy})")

if __name__ == '__main__':
    eval_bot()
