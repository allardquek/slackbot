"""
Main script to run the Slack bot.
"""
import slack
import os
import pandas as pd
import pickle
import random

from dotenv import load_dotenv
from helpers import create_logger, preprocess, train_bot
from helpers import GREETINGS, MIN_CONFIDENCE, DEFAULT_REPLY, GREETING_REPLY
from flask import Flask, request, Response
from pathlib import Path
from slackeventsapi import SlackEventAdapter


logger = create_logger()
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)
SLACK_TOKEN = os.environ['SLACK_TOKEN']
SIGNING_SECRET = os.environ['SIGNING_SECRET']

app = Flask(__name__)
slack_event_adapter = SlackEventAdapter(SIGNING_SECRET, '/slack/events', app)
client = slack.WebClient(token=SLACK_TOKEN)
BOT_ID = client.api_call("auth.test")["user_id"]

# * Create a new instance of a ChatBot
bot = train_bot('Ai Palette Bot', 'data/questions.csv')
answers = pd.read_csv('data/answers.csv').to_dict()
with open('data/jokes.txt', 'rb') as f:
    jokes_dict = pickle.load(f)
    JOKES_LIST = list(jokes_dict.items())


@slack_event_adapter.on('message')
def handle_message(payload):
    event = payload.get('event', {})
    user_id = event.get('user')

    # Ignore messages from the bot itself or if new users join
    if user_id == BOT_ID or event.get('subtype') == 'channel_join':
        return

    channel_id = event.get('channel')
    question = event.get('text')
    
    # * Get response from trained model
    processed_qn = preprocess(question)
    if processed_qn in GREETINGS:
        client.chat_postMessage(channel=channel_id, text=GREETING_REPLY)
        return

    answer = bot.get_response(processed_qn, read_only=True)
    ans = answer.text
    if ans.isdigit():
        ans = answers['text'][int(ans) - 1]

    if answer.confidence < MIN_CONFIDENCE:
        ans = DEFAULT_REPLY

    client.chat_postMessage(channel=channel_id, text=ans)

    logger.info(f"Question asked: {question}")
    logger.info(f"Question processed: {answer.in_response_to}")
    logger.info(f"Answer retrieved: {ans} (Confidence: {answer.confidence})")
    return Response(), 200


@app.route('/joke', methods=['POST'])
def joke():
    """
    Sends the user a random food joke.
    """
    client.chat_postMessage(channel="#jokes", text="Serving a joke right up! 🤔")
    random_joke = random.choice(JOKES_LIST)
    joke = random_joke[0] + '\n' + random_joke[1]

    client.chat_postMessage(channel="#jokes", text=joke)
    return Response(), 200


@app.route('/help', methods=['POST'])
def help():
    """
    To enable /help, remember to update url at
    https://api.slack.com/apps/A033VS2SKFY/slash-commands
    """
    data = request.form
    channel_id = data.get('channel_id')
    user_id = data.get('user_id')
    help_text = "This bot is still in development. Some features may be buggy 😬"
    client.chat_postMessage(channel="#private-test", text=help_text)
    client.chat_postMessage(channel=channel_id, text=help_text)
    client.chat_postMessage(channel=f'@{user_id}', text=help_text)
    return Response(), 200


if __name__ == "__main__":
    app.run(debug=True)
