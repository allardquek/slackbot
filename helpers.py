"""
Helper functions for `bot.py`, including functions to:
    - Create logger
    - Train the bot
    - Preprocess the questions
"""
import csv
import logging
import nltk
import re

from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
nltk.download('omw-1.4')

GREETINGS = ['hi', 'hello', 'sup', 'yo', 'hey', 'start']
MIN_CONFIDENCE = 0.5
DEFAULT_REPLY = "Sorry, I don't have a good answer to this, but I'm learning!"
GREETING_REPLY = "Welcome! I'm a Nusak from Ai Palette and I am here to help you find answers to your questions. How can I help you?"


def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    return tag_dict.get(tag, wordnet.NOUN)


def lem_sentence(s, lemmatizer):
    text_tokens = word_tokenize(s)
    lem_list = [lemmatizer.lemmatize(token, get_wordnet_pos(token)) for token in text_tokens]
    return ' '.join(lem_list)


def preprocess(question, lemmatizer=WordNetLemmatizer()):
    question = question.lower()                         # Case folding
    question = re.sub('[^A-Za-z0-9 ]+', '', question)   # Remove punctuation
    question = lem_sentence(question, lemmatizer)       # Lemmatize
    return question


def train_bot(name, questions_file):
    """
    Trains a chatterbot of given name using the questions_file provided.
    """
    bot = ChatBot(
        name,
        preprocessors=[
            'chatterbot.preprocessors.clean_whitespace'
        ],
        logic_adapters=[
            {
                'import_path': 'chatterbot.logic.BestMatch',
                'default_response': DEFAULT_REPLY,
                'maximum_similarity_threshold': MIN_CONFIDENCE
            }
        ]
    )

    trainer = ListTrainer(bot)

    with open(questions_file) as csvfile:
        rows = csv.reader(csvfile)
        next(rows)                  # Skip the header row   
        TRAIN_DATA = list(rows)     # Pass reader object to list() to get a list of lists

    processed_qns = [preprocess(qn[0]) for qn in TRAIN_DATA]

    for processed_qn, subList in zip(processed_qns, TRAIN_DATA):
        subList[0] = processed_qn    
        
    for data in TRAIN_DATA:
        print(data)
        trainer.train(data)

    return bot


def create_logger():
    """
    Creates a custom logger for the bot.
    """
    logger = logging.getLogger(__name__)         # Logger's name will be __main__ if run directly but name of file if imported
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    # Remember FileHandler! To tell new logger which file to log to and set mode to 'write'
    file_handler = logging.FileHandler('chatbot.log', mode='w')
    file_handler.setLevel(logging.INFO)          # Set level for data to be written to file (ignore logs below ERROR level)
    file_handler.setFormatter(formatter)

    # Create a StreamHandler to allow logs to be displayed in console while writing higher level logs to file
    stream_handler = logging.StreamHandler()     # No need to setLevel as already set above with logger
    stream_handler.setFormatter(formatter)       # To set same format as logs written to file

    # Remember to add any new Handlers created
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)     

    return logger
    