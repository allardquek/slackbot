# Ai Palette Chatbot

_Short-term objective:_ Create a Data Science Team chatbot to answer frequently asked questions

_Long-term objective:_ Expand the chatbot for customers’ use

For more resources such as tutorials and approaches: please read [`resources/Resources.md`](resources/Resources.md).

## Description of Files/Folders

- `resources/`: folder for resources such as tutorials and experiemental code
- `.env_sample`: template for configuring envrionment variables
- `bot.py`: main script to run bot on Slack
- `eval.py`: script to test the bot on test data
- `helpers.py`: contains helper functions for `bot.py`
- `index_file.py`: script to pickle csv files into dictionaries which can be read in and used in `bot.py`
- `answers.csv`: contains actual answers to corresponding to answer IDs
- `jokes.csv`: contains list of jokes in `question,answer` format
- `questions.csv`: contains FAQ questions and corresponding answer IDs
- `Procfile`: experimental configuration for deployment to Heroku (not used)
- `requirements.txt`: contains list of packages required for this project

Note that log files will be created when running `bot.py` and `eval.py` (`chatbot.log` and `eval.log` respectively). These files contain the logs when the respective scripts are run and could be helpful in debugging and understanding the results obtained.

## Set up

1. Replace the template envrionment variables in `.env-sample` with the actual values.
   The actual envrionment variables can be found in the datascience@aipalette.com Google Drive file name "Confluence".
2. Rename the `.env-sample` file to `.env`.

## Running the Bot

1. You can use [ngrok](https://ngrok.com/) to set up an URL to your localhost server
1. Once ngrok is installed, run `ngrok http 5000`.
1. Copy the given URL provided (either http or https, shouldn't matter)
1. Go to the the [Slack API page](https://api.slack.com/apps/) and select the app for this bot
1. Under Event Subscriptions, replace the Request URL with the URL copied in step 4
1. Start the bot by running `python bot.py`
1. Start sending messages on Slack to your bot

**Note:** There are other steps for creating the Slack app, as well as setting up the bot in your workspace (e.g. adding the bot to the channel). The above steps assume all this has already been correctly set up.
